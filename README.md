# SMX M04 Sistemes Operatius en Xarxa
## UF2 Sistemes Operatius lliures en xarxa
Curs 2019 - 2020

Descripció 
 * Programació i criteris d'avaluació [ smx2_m04.pdf ]
 * Programació curs anterior [ PRG_SMX_SistemesOperatiusXarxa_2018-19(1).doc ]

Activitats
 * Activitat 1.1: Breu estudi de requeriments i porpòsit dels sistemes operatius lliures de xarxa [ m04uf2act1p1.pdf ]
 * Activitat 1.2: Pràctiques de particionament [ m04uf2act1p2.pdf ]
 * Activitat 1.3: Pràctiques d'instal·lació inicial de Zentyal [ m04uf2act1p3.pdf ]
 * Activitat 1.4: Càlculs de xarxa. Configuració de xarxa [ m04uf2act1p4.pdf ]
 * Activitat 1.5: Actualització de programari (paquets i repositoris)
                                                                                          
